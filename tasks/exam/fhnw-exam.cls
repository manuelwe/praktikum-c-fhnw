\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fhnw-exam}[2024/05/02 Prüfungsaufgabe für C++ Kurs an der FHNW]

\LoadClass[head=Prüfungsaufgabe,number]{../fhnw}

\newfhnwenvironment{aufgabe}{Prüfungsaufgabe}{\fhnwstift}
\newfhnwenvironment{loesung}{Musterlösung}{\fhnwhaken}
