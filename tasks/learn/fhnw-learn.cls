\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fhnw-lernaufgabe}[2024/05/01 Lernaufgabe für C++ Kurs an der FHNW]

\LoadClass[head=Lernaufgabe]{../fhnw}

\newfhnwenvironment{aufgabe}{Lernaufgabe}{\fhnwflasche}
