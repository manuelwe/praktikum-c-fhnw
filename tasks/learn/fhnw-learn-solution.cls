\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fhnw-lernloesung}[2024/05/01 Musterlösung zur Lernaufgabe für C++ Kurs an der FHNW]

\LoadClass[head={Musterlösung zur Lernaufgabe}]{../fhnw}

\newfhnwenvironment{loesung}{Musterlösung}{\fhnwhaken}
