\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fhnw-sheet}[2024/05/02 Arbeitsblatt für C++ Kurs an der FHNW]

\LoadClass[head=Arbeitsblatt,number]{../fhnw}

\newfhnwenvironmentcounted{kontrollaufgabe}{Aufgabe}{\fhnwstift}
\newfhnwenvironmentcounted{lernaufgabe}{Aufgabe}{\fhnwflasche}
