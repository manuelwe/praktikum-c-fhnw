\documentclass{fhnw-talk}

\fhnwTitel{Funktionen-Templates}

\begin{document}

  \maketitle
  
  Wir geben eine kurze Einführung in die Grundbegriffe des Template-Mechanismus anhand von sogenannten Funktionen-Templates.
  Diese erlauben es, generische Funktionen in C++ zu schreiben.
  Auf verwandte Begriffe wie beispielsweise Klassen-Templates oder auch auf weiterführende Konzepte wie variadische Templates wird hier nicht eingegangen.
  
  \section*{Einleitung}
  
  Stellen Sie sich vor, Sie möchten eine Funktion~\codeinline{minimum}, die von zwei gegebenen Zahlen vom Typ~\codeinline{int} die kleinere auswählt.
  Nichts leichter als das, denken Sie sich, und schreiben folgenden Code.
  \begin{code}
int minimum(int x, int y) {
  if(x < y) return x;
  else      return y;
}
  \end{code}
  
  Später bemerken Sie, dass an anderer Stelle im Programm zwar mit Fliesskommazahlen gerechnet wird, aber dort die Bestimmung der kleineren von zwei Zahlen genauso von Interesse ist.
  Dies ist aber nicht weiter schwer und dank Überladung darf sogar der gleiche Name für die Funktion verwendet werden.
  \begin{code}
double minimum(double x, double y) {
  if(x < y) return x;
  else      return y;
}
  \end{code}
  
  Nach kurzer Euphorie fällt Ihnen aber auf, dass Sie gerade einen der wichtigsten Grundsätze von gutem Software-Design verletzt haben:
  Sie haben sich wiederholt.
  Sie haben im Prinzip zweimal das genau Gleiche geschrieben.
  Um Wiederholungen dieser und anderer Art vermeiden zu können, wurde der Template-Mechanismus in C++ eingeführt.
  Sogenannte Funktionen-Templates erlauben es, eine Funktion wie~\codeinline{minimum} generisch für alle Arten von Zahlen auf einmal zu implementieren.
  
  \section*{Definition und Instanziierung}
  
  Die generische Definition der Funktion~\codeinline{minimum} aus der Einleitung -- das heisst jetzt die Definition des \emph{Funktionen-Templates}~\codeinline{minimum} -- sieht wie folgt aus.
  \begin{code}
template <typename T>
T minimum(T x, T y) {
  if(x < y) return x;
  else      return y;
}
  \end{code}
  
  Es fällt auf, dass die konkreten Typen wie~\codeinline{int} und~\codeinline{double} verschwunden und stattdessen mit einem \emph{Typ-Parameter}~\codeinline{T} ersetzt worden sind.
  Zusätzlich wird in der neuen ersten Zeile ausgedrückt, dass es sich hierbei um ein Template handelt und dass es sich bei~\codeinline{T} um einen beliebigen substituierbaren Typ handelt.
  
  Dieses definierte Funktionen-Template kann jetzt auf unterschiedliche Arten aufgerufen werden.
  \begin{code}
std::cout << minimum(2, 3);          // Ausgabe: 2
std::cout << minimum(2.5, 3.5);      // Ausgabe: 2.5
std::cout << minimum<int>(2.5, 3.5); // Ausgabe: 2
  \end{code}
  
  Der Compiler bemerkt beispielsweise in der ersten Zeile, dass die Argumente~\codeinline{2} und~\codeinline{3} den Typ~\codeinline{int} haben und dass deshalb das Funktionen-Template mit dem Typ~\codeinline{T = int} instanziiert werden soll.
  Eine \emph{Instanziierung} eines Templates ist nichts anderes als das Einsetzen eines konkreten Typs wie~\codeinline{int} bei jedem Typ-Parameter~\codeinline{T}.
  Die durch dieses Einsetzen entstandene Funktion wird dann kompiliert, die übliche Typprüfung wird durchgeführt und entsprechender Code für die Binärdatei wird generiert.
  
  In der zweiten Zeile mit Argumenten~\codeinline{2.5} und~\codeinline{3.5} vom Typ~\codeinline{double} ist der Vorgang im Prinzip der gleiche, ausser dass das Template mit~\codeinline{T = double} instanziiert wird.
  Es ist jedoch wichtig, zu verstehen, dass durch diese zwei unterschiedlichen Instanziierungen auch zwei unterschiedliche Kopien der Funktion~\codeinline{minimum} in der Binärdatei angelegt werden.
  Diese Binärdatei sieht also ungefähr gleich aus, wie wenn wir zwei explizite Überladungen der Funktion~\codeinline{minimum} (so wie in der Einleitung) geschrieben hätten.
  
  In der dritten Zeile schliesslich wird mit eckigen Klammern explizit angegeben, welche Instanziierung des Funktionen-Templates aufgerufen werden soll.
  Das heisst, obwohl beide Argumente vom Typ~\codeinline{double} sind, wird die Instanziierung mit~\codeinline{T = int} bevorzugt.
  Dies hat zur Folge, dass die Argumente zuerst nach~\codeinline{int} konvertiert werden, was die beobachtete Ausgabe erklärt.
  Da das Template mit~\codeinline{T = int} schon einmal instanziiert wurde, wird hier aber kein weiterer Code für die Binärdatei generiert.
 
  \section*{Spezialisierung}
  
  Die generische Definition des Funktionen-Templates~\codeinline{minimum} erlaubt für jeden Typ~\codeinline{T}, das Minimum zu berechnen, solange zwei Werte vom Typ~\codeinline{T} mit~\codeinline{operator<} verglichen werden können.
  Was wäre jetzt aber, wenn für einen bestimmten Typ (wie zum Beispiel~\codeinline{char}) der Vergleichsoperator bereits implementiert ist, wir aber nicht ganz einverstanden sind damit, wie dieser Vergleich durchgeführt wird?
  
  Beispielsweise gilt~\codeinline{'Z' < 'a'}, weil die Grossbuchstaben in der ASCII-Tabelle vor den Kleinbuchstaben aufgeführt werden.
  Es könnte jedoch sein, dass wir gar nicht an Gross- und Kleinschreibung interessiert sind, sondern nur an der Reihenfolge im Alphabet.
  Wir könnten jetzt natürlich die Argumente vor jedem Aufruf der Funktion mit Hilfe von~\codeinline{std::tolower} in Kleinbuchstaben umwandeln.
  Das ist aber ein bisschen mühsam und es wäre schöner, wenn die Funktion diese Umwandlung gleich selber macht.
  Und genau das ist auch möglich mit einer \emph{Template-Spezialisierung}.
  
  \begin{code}
template <>
char minimum<char>(char x, char y) {
  if(std::tolower(x) < std::tolower(y)) return x;
  else                                  return y;
}
  \end{code}
  
  Mit dem obigen Code drücken wird aus, dass wir für~\codeinline{T = char} nicht die generische Version des Funktionen-Templates aufrufen wollen.
  Stattdessen soll die im gleichen Atemzug definierte spezielle Version~\codeinline{minimum<char>} verwendet werden.
  Diese spezielle Version führt vor dem Vergleich die gewünschte Umwandlung in Kleinbuchstaben aus und gibt dann den so gefundenen Buchstaben zurück.
  
  \section*{Mehrere Typ-Parameter}
  
  Wenden wir uns zur Abwechslung einem anderen Beispiel zu.
  Stellen Sie sich vor, Sie haben einen Behälter~\codeinline{c} vom Typ~\codeinline{std::vector<int>} und Sie möchten wissen, ob ein spezifischer Wert wie beispielsweise~\codeinline{x = 7} darin vorkommt.
  Um dieses Problem zu lösen, schreiben Sie eine Funktion~\codeinline{search}, die den Behälter~\codeinline{c} und den Wert~\codeinline{x} als Argumente übergeben bekommt und dann einfach der Reihe nach alle Einträge von~\codeinline{c} mit~\codeinline{x} vergleicht.
  
  Etwas später aber bemerken Sie, dass Sie für einen zweiten Behälter vom Typ~\codeinline{std::array<int, 5>} die genau gleiche Frage beantworten möchten.
  Da Sie mittlerweile schon ein bisschen geübt sind im Umgang mit Funktionen-Templates, schreiben Sie ohne grosse Mühe und Anstrengung folgende generische Funktion, die das Problem für beide Behälter auf einmal zu lösen vermag.
  
  \begin{code}
template <typename Container>
bool search(const Container& c, int x) {
  for(size_t i = 0; i < c.size(); ++i) {
    if(c[i] == x) return true;
  }
  return false;
}
  \end{code}
  
  Erfreut stellen Sie fest, dass man für einen Typ-Parameter wie~\codeinline{Container} offenbar auch längere Bezeichner verwenden darf, was den Code durchaus lesbarer macht, weil er direkt ausdrückt, dass das erste Argument der Funktion eine Art Behälter sein soll.
  Ebenso beobachten Sie, dass man den generischen Typ des Funktionsarguments beliebig verzieren darf, beispielsweise mit~\codeinline{const} oder mit~\codeinline{&}.
  
  Was Ihnen aber etwas sauer aufstösst ist das zweite Funktionsargument vom Typ~\codeinline{int}.
  Wieso gerade~\codeinline{int}?
  Wieso nicht~\codeinline{double}?
  Es könnte ja sein, dass ein Behälter mit Fliesskommazahlen untersucht werden muss.
  Dies ist aber kein grösseres Problem, denn Sie können das Funktionen-Template ganz einfach auf zwei Typ-Parameter erweitern, so dass es sowohl im Typ des Behälters~\codeinline{c} als auch im Typ des Werts~\codeinline{x}, nach dem gesucht wird, generisch ist.
  
  \begin{code}
template <typename Container, typename Value>
bool search(const Container& c, Value x) {
  for(size_t i = 0; i < c.size(); ++i) {
    if(c[i] == x) return true;
  }
  return false;
}
  \end{code}
  
  \section*{Kurzform}
  
  Die verwendete Syntax mit~\codeinline{template <...>} kann zuweilen etwas sperrig wirken.
  Mit dem schon bekannten Schlüsselwort~\codeinline{auto} existiert aber eine Kurzform für Funktionen-Templates, die in gewissen einfachen Fällen angewendet werden kann.
  Das Funktionen-Template~\codeinline{search} aus dem vorhergehenden Abschnitt kann beispielsweise auf folgende vollkommen äquivalente Art und Weise geschrieben werden.
  \begin{code}
bool search(const auto& c, auto x) {
  for(size_t i = 0; i < c.size(); ++i) {
    if(c[i] == x) return true;
  }
  return false;
}
  \end{code}
  
  Das heisst, alle Typ-Parameter von den Funktionsargumenten werden einfach mit~\codeinline{auto} ersetzt.
  Dies geht solange gut, bis einer der Typ-Parameter auch anderswo vorkommt, zum Beispiel im Funktionenkörper.
  Auch wäre man nicht gut beraten, diese Kurzform für das Funktionen-Template~\codeinline{minimum} von früher einzusetzen.
  Der Unterschied ist subtil, aber weil jedes Vorkommen von \codeinline{auto} als eigener und unabhängiger Typ-Parameter interpretiert wird, entspräche die Kurzform von~\codeinline{minimum} folgender Definition.
  \begin{code}
template <typename T1, typename T2>
auto minimum(T1 x, T2 y) {
  if(x < y) return x;
  else      return y;
}
  \end{code}
  
  Der Typ des Rückgabewerts der Funktion wurde mit Absicht bei~\codeinline{auto} belassen.
  Was sollte er denn sonst sein?
  Weder~\codeinline{T1} noch~\codeinline{T2} ergibt einen Sinn, weil sowohl der Wert~\codeinline{x} als auch der Wert~\codeinline{y} zurückgegeben werden könnte.
  Falls dieses Template jetzt aber mit zwei verschiedenen Typen instanziiert wird, beispielsweise als~\codeinline{minimum<int, double>}, dann führt dies immer zu einem Kompilierfehler, weil eben genau dieser Typ des Rückgabewerts nicht mehr eindeutig bestimmt werden kann.
  
  \section*{Default-Argumente}
  
  Kommen wir nun zum dritten und letzten Beispiel.
  Folgendes Funktionen-Template gibt der Reihe nach alle Werte eines Behälters vom Typ~\codeinline{std::vector<T>} mit  generischem~\codeinline{T} aus.
  Durch die Angabe eines zweiten Typ-Parameters~\codeinline{S} soll dem Benutzer aber die Möglichkeit gegeben werden, die enthaltenen Werte kurz vor deren Ausgabe noch in einen anderen Typ~\codeinline{S} umzuwandeln.
  \begin{code}
template <typename T, typename S>
void print(const std::vector<T>& v) {
  for(const T& x : v)
    std::cout << static_cast<S>(x) << " ";
}
  \end{code}
  
  Dieses Template funktioniert zwar wie gewünscht, es ist aber in seiner Anwendung ein bisschen unhandlich.
  Der Grund dafür ist, dass der Programmierer jetzt immer einen expliziten Aufruf inklusive Typ-Parameter-Liste schreiben muss; sogar dann, wenn er von der Typ-Umwandlung vor der Ausgabe gar keinen Gebrauch machen möchte.
  Hat er beispielsweise einen Behälter~\codeinline{v} vom Typ~\codeinline{std::vector<int>} und möchte er alle enthaltenen Werte ohne Typ-Umwandlung ausgeben, dann muss er trotzdem jedes Mal den expliziten Aufruf~\codeinline{print<int, int>(v)} schreiben.
  Wenn er nämlich nur~\codeinline{print(v)} schreiben würde, dann hätte der Compiler bei der Instanziierung nicht genügend Information, um den Typ-Parameter~\codeinline{S} eindeutig zu bestimmen.
  Dies liegt daran, dass der Typ des einzigen Funktionsarguments~\codeinline{v} in keiner Art und Weise von~\codeinline{S} abhängig ist.
  
  Mit sogenannten~\emph{Default-Template-Argumenten} kann aber die Handhabung des obigen Funktionen-Templates vereinfacht werden, so dass der Aufruf~\codeinline{print(v)} funktioniert und auch keine Umwandlung der enthaltenen Werte vornimmt.
  Ähnlich wie bei Default-Argumenten von normalen Funktionen kann dadurch ein Wert (oder jetzt hier ein Typ) vordefiniert werden, der genau dann verwendet wird, wenn der Programmierer selber gar kein Argument übergeben hat.
  Die natürliche Wahl für diesen vordefinierten Typ für~\codeinline{S} ist offenbar~\codeinline{T}.
  Diese Wahl stellt sicher, dass die Typ-Umwandlung von~\codeinline{T} nach~\codeinline{S = T} gar keinen Effekt hat.
  \begin{code}
template <typename T, typename S = T>
void print(const std::vector<T>& v) {
  for(const T& x : v)
    std::cout << static_cast<S>(x) << " ";
}
  \end{code}
  
  \section*{Überladen}
  
  Genauso wie normale Funktionen können auch Funktionen-Templates überladen werden.
  Stellen Sie sich vor, Sie möchten die Definition von~\codeinline{print} von vorher auf einen anderen Typ von Behälter erweitern, wie beispielsweise~\codeinline{std::map<K, T>}.
  Dies können Sie tun, indem Sie einfach eine zweite generische Definition des Funktionen-Templates einführen, das aber andere Argument-Typen hat.
  
  Gleichzeitig können Sie (ähnlich wie bei der schon behandelten Template-Spezialisierung) beliebige Änderungen an der Implementation der Funktion anbringen.
  Weil sich die beiden generischen Typen~\codeinline{std::vector<T>} (was eine Sequenz von Werten repräsentiert) und~\codeinline{std::map<K, T>} (was eine Menge von Schlüssel-Wert-Paaren repräsentiert) in ihrem Wesen und auch in ihrer Handhabung grundlegend unterscheiden, führt auch nichts an einer adäquaten Anpassung des Codes vorbei.
  \begin{code}
template <typename K, typename T, typename S = T>
void print(const std::map<K, T>& m) {
  for(const std::pair<K, T>& p : m)
    std::cout << p.first << ":" << static_cast<S>(p.second) << " ";
}
  \end{code}
  
\end{document}
