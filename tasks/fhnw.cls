\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fhnw}[2024/05/01 Generische Klasse für C++ Kurs an der FHNW]

\RequirePackage{kvoptions}
\DeclareStringOption[Fehlende Option: head]{head}
\DeclareBoolOption{number}
\ProcessKeyvalOptions*

\def\@title{Set title with \texttt{\\fhnwTitel}}
\newcommand\fhnwTitel[1]{
  \def\@title{#1}
}

\iffhnw@number
  \def\@number{???}
  \newcommand\fhnwNummer[1]{
    \def\@number{#1}
  }
\fi

\LoadClass[a4paper]{article}

\RequirePackage[utf8]{inputenc}
\RequirePackage[margin=2.5cm]{geometry}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage[german]{babel}
\RequirePackage[usenames,dvipsnames]{xcolor}
\RequirePackage{graphicx}
\RequirePackage{amsmath}
\RequirePackage{amssymb}

\RequirePackage{fancyhdr}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhead[L]{\includegraphics[scale=1]{../fhnw_10mm}}
\fancyhead[R]{\textsf{\textbf{Programmieren in C++\\[3pt]\fhnw@head\iffhnw@number~\@number\fi\\[-10pt]}}}
\fancyfoot{}

\RequirePackage{fontawesome}
\newcommand\fhnw@newsymbol[2]{\def#1{\makebox[0.5cm][c]{\raisebox{-0.5pt}[0pt][0pt]{#2}}}}
\fhnw@newsymbol\fhnwstift\faPencilSquareO
\fhnw@newsymbol\fhnwflasche\faFlask
\fhnw@newsymbol\fhnwhaken\faCheckSquareO

\RequirePackage{listings}
\lstdefinestyle{cpp}{%
  language=C++,
  morekeywords={constexpr,nullptr,override,concept,requires},
  framexleftmargin=2pt,
  framexrightmargin=2pt,
  framextopmargin=1pt,
  framexbottommargin=1pt, 
  frame=tb,
  framerule=0pt,
  framesep=2pt,
  basicstyle=\ttfamily,
  keywordstyle=\bfseries\color{RoyalBlue},
  commentstyle=\color{OliveGreen},
  stringstyle=\color{RawSienna},
  showstringspaces=false,
  showlines=true,
  %identifierstyle=\color{RawSienna},
  backgroundcolor=\color{white!95!black},
}
\lstdefinestyle{cppnumbered}{%
  style=cpp,
  numbers=left,
  numberstyle=\tiny\color{white!50!black},
  firstnumber=auto,
}
\lstnewenvironment{code}{\lstset{
  style=cpp,
}}{}
\lstnewenvironment{codeT}[1]{\lstset{
  style=cpp,
  title={\ttfamily#1},
}}{}
\lstnewenvironment{codeN}{\lstset{
  style=cppnumbered,
}}{}
\newcommand\codeinline[1]{\lstinline[style=cpp]{#1}}

\renewcommand{\maketitle}{%
  \begingroup%
    \par\vspace*{-5pt}%
    \bfseries\sffamily\Huge%
    \noindent\@title%
    \par\vspace{20pt}%
  \endgroup%
}

\newcounter{fhnwcounter}
\newcommand\newfhnwenvironmentcounted[3]{
  \newenvironment{#1}{
    \refstepcounter{fhnwcounter}
    \medskip\noindent\textbf{\textsf{\hspace{-2pt}#3\;#2 \@number.\arabic{fhnwcounter}}}
  }{
    \medskip
  }
}
\newcommand\newfhnwenvironment[3]{
  \newenvironment{#1}{
    \medskip\noindent\textbf{\textsf{\hspace{-2pt}#3\;#2}}
  }{
    \medskip
  }
}

\RequirePackage{tikz}
\tikzset{
  memory/.style={
    rectangle,draw=RoyalBlue,fill=white!95!black,thick,minimum width=20pt,minimum height=20pt,
    prefix after command= {\pgfextra{\tikzset{every label/.style={label position=below,label distance=0pt}}}}
  },
  pointer/.style={->,thick,shorten >=5pt}
}

%\ifdefined\HCode
%  \def~{ }
%  \def\symbolKontrollaufgabe{ }
%  \def\symbolLernaufgabe{ }
%  \def\symbolLoesung{ }
%  \def\titelKontrollaufgabe{Quiz}
%  \def\titelLernaufgabe{Lernaufgabe}
%  \def\titelLoesung{Lösung zu Aufgabe}
%  \lstnewenvironment{codeN}{\lstset{
%    style=cpp,
%  }}{}
%  \lstnewenvironment{codeNT}[1]{\lstset{
%    style=cpp,
%    title={\ttfamily#1},
%  }}{}
%\else
%  \lstnewenvironment{codeN}{\lstset{
%    style=cppnumbered,
%  }}{}
%  \lstnewenvironment{codeNT}[1]{\lstset{
%    style=cppnumbered,
%    title={\ttfamily#1},
%  }}{}
%\fi
