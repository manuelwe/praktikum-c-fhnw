\documentclass{fhnw-preparation}

\usepackage{enumitem}

\fhnwTitel{Vorbereitung der Prüfungslektionen}

\begin{document}
  
  \maketitle
  
  Dieses Dokument enthält die schriftliche Vorbereitung des DZ-Studenten Manuel Wettstein für die beiden Prüfungslektionen vom 4.\ Juni 2024 an der FHNW.

  Das übergeordnete Lernziel von beiden Prüfungslektionen ist eine Vertiefung der Thematik über \emph{Container, Iteratoren und Algorithmen} in C++, die bereits in der vorhergehenden Woche am 28.\ Mai 2024 ein erstes Mal behandelt wurde:
  \begin{itemize}
    \item
      \textbf{Prüfungslektion 1:}
      Zum einen soll die Ausdruckskraft der bereits bekannten und auf Iteratoren basierten Algorithmen-Bibliothek von C++ durch Kombination mit Funktionen und Prädikaten weiter verstärkt werden.
      Zu diesem Zweck werden die Konzepte des \emph{Funktors} und der \emph{Lambda-Funktion} eingeführt und deren Anwendung mit einer Auswahl von Algorithmen vorgezeigt.
    \item
      \textbf{Prüfungslektion 2:}
      Zum anderen wird ein Einblick in die sogenannte \emph{Ranges-Bibliothek} gegeben, die als Alternative oder gar als Ersatz für die bereits behandelten Iteratoren verstanden werden kann.
      Neben der Behandlung einer Auswahl von Beispielen soll auch auf die Vorteile von Ranges gegenüber dem bereits bekannten Konzept des Iteratoren-Paars eingegangen werden.
  \end{itemize}
  Weil die Thematiken beider Prüfungslektionen eng verzahnt sind und auf dem gleichen Vorwissen aufbauen, habe ich mich dazu entschieden, die zwei Lektionen mit einem für beide gleichermassen relevanten informierenden Unterrichtseinstieg zu beginnen.
  Deshalb wird die Vorbereitung dieses informierenden Unterrichtseinstiegs im Folgenden getrennt von den beiden eigentlichen Prüfungslektionen aufgeführt. 

  \setcounter{section}{-1}
  \section{Informierender Unterrichtseinstieg}
  
  Dieser informierende Unterrichtseinstieg dient, wie oben schon einmal erwähnt, als gemeinsamer Einstieg für beide Prüfungslektionen.
  Ein grober Überblick über die zu behandelnden Themen soll gegeben werden und das für beide Lektionen relevante Vorwissen soll aktiviert werden.
  
  \subsection{Bild der Studierenden}
  \label{0:vorwissen}
  
  \begin{enumerate}[label=\alph*)]
    \item
      Vorkenntnisse:
      Die Studierenden haben in den drei Unterrichtslektionen in der vorhergehenden Woche am 28.\ Mai 2024 die drei eng zusammengehörenden Themen \emph{Container}, \emph{Iteratoren} und \emph{Algorithmen} kennengelernt.
      
      Bei den \emph{Containern} wurde auf eine repräsentative Auswahl bestehend aus~\codeinline{std::vector}, \codeinline{std::list}, \codeinline{std::set} und einige weitere eingegangen.
      Spezifische Unterschiede in der Handhabung dieser Container wurden ebenso diskutiert.
      Es darf also davon ausgegangen werden, dass eine Vertrautheit im Umgang mit diesen Containern (insbesondere mit~\codeinline{std::vector}, der schon über das ganze Semester immer wieder im Unterricht verwendet wurde) bei allen Studierenden vorhanden ist.
      
      Das Konzept des \emph{Iterators} wurde als Verallgemeinerung eines Zeigers auf ein primitives Array (ein Objekt, das den Studierenden schon seit Anfang des Semesters bekannt ist) eingeführt.
      Die unterschiedlichen Kategorien~\codeinline{RandomAccessIterator},~\codeinline{BidirectionalIterator} und~\codeinline{ForwardIterator} wurden benannt.
      Sie wurden anhand eines konkreten und der Kategorie entsprechenden Containers motiviert.
      Des Weiteren wurden etwaige Limitationen der einzelnen Kategorien anhand von konkreten Algorithmen aufgezeigt.
      Insbesondere der Umgang mit den Iteratoren von~\codeinline{std::vector} (das heisst, Iteratoren der Kategorie~\codeinline{RandomAccessIterator}, die keinerlei Limitationen aufweist) sollte den Studierenden deshalb keine grosse Mühe mehr bereiten.
      
      Bei den \emph{Algorithmen} wurde eine eher kleine Auswahl bestehend aus~\codeinline{std::fill}, \codeinline{std::reverse}, \codeinline{std::copy} und einigen weiteren thematisiert.
      Der Anspruch war offenbar nicht, einen vollständigen Überblick über alle in der Standard-Bibliothek enthaltenen Algorithmen zu erlangen.
      Vielmehr ging es darum, eine möglichst diverse Auswahl von Algorithmen mit möglichst unterschiedlichen Funktions-Signaturen zu betrachten.
      Den Studierenden sollte deshalb gut bekannt sein, dass diese Algorithmen zwar fast immer mit einem Iteratoren-Paar angesteuert werden, dass sie aber dennoch oft nach weiteren Argumenten verlangen, wie beispielsweise Werte oder zusätzliche Iteratoren.
    \item
      Klassenspezifisches:
      Die ungefähr neun Studierenden, die regelmässig am Unterricht teilgenommen haben, haben sich von allen früheren Unterrichtswochen bereits an einen informierenden Unterrichtseinstieg gewöhnt, so wie er auch jetzt in diesem Abschnitt noch entwickelt wird.
      Insbesondere sind sie vertraut damit, dass die Unterrichtssequenz jeweils mit einer circa 5- bis 15-minütigen Themen-Übersicht (inklusive dem Hinweis auf relevantes Vorwissen) an der Wandtafel beginnt.
      
      Direkt folgend auf die Diskussion an der Wandtafel erwarten sie ein interaktives Quiz mit ungefähr fünf Multiple-Choice-Fragen.
      Die Studierenden sind darin geübt, diese Fragen auf ihrem Computer oder Mobiltelephon zu beantworten, was direkt danach ohne Verzögerung eine anonyme Auswertung der Resultate erlaubt.
      Bei aufgedeckten systematischen Fehlvorstellungen wird natürlich im Plenum noch einmal auf die entsprechende Frage eingegangen.
      
      Da der Unterricht aber jeweils aus drei aufeinanderfolgenden und thematisch zusammengehörenden Lektionen bestand, war es in vergangenen Wochen immer so, dass dieser Unterrichtseinstieg jeweils am Anfang der ersten von insgesamt drei Lektionen gemacht wurde.
      Weil jetzt aber nur die zwei späteren Lektionen eine thematische Einheit formen, wird der Unterrichtseinstieg ausnahmsweise am Anfang der zweiten Lektion (das heisst, am Anfang der ersten Prüfungslektion) durchgeführt.
      Diese kleine Umstellung sollte den Studierenden allerdings keine grosse Mühe bereiten.
      
  \end{enumerate}
  
  \subsection{Sachanalyse}
  
  Die Sachanalyse zu den zwei neuen Themenblöcken wird später separat in den entsprechenden Abschnitten~\ref{1:fachlich} und~\ref{2:fachlich} von den beiden Prüfungslektionen durchgeführt.
  
  \subsection{Lernziele}

  Da es sich bei diesem informierenden Unterrichtseinstieg hautpsächlich um eine Repetition von bereits bekanntem Wissen und um einen groben Ausblick auf die darauffolgenden zwei Lektionen handelt, werden an dieser Stelle noch keine konkreten Lernziele formuliert.
  
  \subsection{Didaktisch-methodische Überlegungen}
  
  Dieser informierende Unterrichtseinstieg besteht im Wesentlichen aus den folgenden zwei Komponenten:
  Erstens, ein kurzer Rückblick und ein Ausblick auf die neuen zu behandelnden Themen an der Wandtafel, mit gleichzeitiger Einordnung der neuen Themen mit bereits bekannten Begriffen. 
  Zweitens, ein anonymes interaktives Quiz mit fünf Multiple-Choice-Aufgaben für die Aktivierung und gleichzeitige Überprüfung von relevantem Vorwissen aus vergangenen Wochen.
 
  \begin{enumerate}[label=\alph*)]
    \item
      Didaktischer Aufbau:
      Anhand des kurzen Rückblicks sollen sich die Studierenden erst einmal die wichtigsten relevanten Begriffe ins Gedächtnis zurückrufen.
      Danach soll anhand von zwei einfach zu verstehenden Beispiel-Algorithmen (\codeinline{std::count_if} mit Prädikat und \codeinline{std::sort} mit Komparator) erkannt werden, wie nützlich die Kombination von Algorithmen mit Prädikaten oder anderen Funktionen sein kann.
      An dieser Stelle sollen auch die Begriffe \emph{Funktor} und \emph{Lambda-Funktion}, das Thema der ersten Lektion, ein erstes Mal beiläufig erwähnt werden.
      Danach werden einige Nachteile des Konzepts des Iteratoren-Paars erwähnt.
      Als Lösung einiger dieser Probleme wird die Idee einer von einem einzelnen Objekt repräsentierten Sequenz präsentiert.
      Der entsprechende Begriff der \emph{Range}, der dann in der zweiten Lektion behandelt wird, wird kurz erwähnt.
      
      Das darauffolgende Quiz beinhaltet fünf relevante Fragen über das Verhalten verschiedener Container, die Manipulation von Iteratoren, und die Funktionsweise von spezifischen Algorithmen.
      Zum einen soll mit diesem Quiz das relevante Vorwissen noch stärker aktiviert werden.
      Zum anderen können potenzielle systematische Fehlvorstellungen direkt im Anschluss in Form eines Klassengesprächs beseitigt werden. 
      
      Die Reihenfolge der beiden Komponenten des informierenden Unterrichtseinstiegs (das heisst, zuerst der Rückblick/Ausblick und erst dann das Quiz) ist bewusst so gewählt worden.
      Da die Studierenden direkt nach dem Ausblick bereits eine grobe Idee davon haben, um was es in den folgenden zwei Lektionen gehen soll, können sie das überprüfte Vorwissen besser einordnen und gleichzeitig schon vorausahnen, in welchem Zusammenhang es relevant sein wird.
    \item
      Methodisches Vorgehen:
      Bei dem Ausblick auf die neuen Themen ist zu bemerken, dass ich mich (wie auch schon in vergangenen Wochen) sehr bewusst für die Verwendung der Wandtafel entschieden habe.
      Zum einen entsteht dadurch ein starker und willkommener Kontrast zum Rest der Unterrichtssequenz, die meistens vom direkten Programmieren und Vorführen am Computer geprägt ist.
      Zum anderen ist die Situation im Unterrichtsraum an der FHNW so, dass bei Verwendung des Projektors die Wandtafel zwar nach unten gefahren wird, dass sie aber trotzdem einigermassen gut sichtbar bleibt für die Studierenden.
      Die Konsequenz davon ist, dass sich die Studierenden auch später noch an dem Aufschrieb an der Wandtafel orientieren können.
      Das Wandtafelbild, das jeweils in den ersten Minuten des Unterrichts erstellt wird (und danach nur noch geringfügig verändert wird), ist deshalb als fortwährende Orientierungshilfe für das behandelte Themengebiet gedacht. 
      
      Beim Quiz fällt der Modus mit den doch sehr einfachen Fragetypen auf.
      Ein offensichtlicher Nachteil davon ist, dass es unmöglich ist, wirklich tiefgreifende Fragen zu formulieren.
      Jedoch wird durch diesen Modus eine sehr zeiteffiziente Überprüfung des Vorwissens von jedem einzelnen Studierenden mit sofortiger Anzeige von Antwort-Statistiken ermöglicht.
      In Windeseile kann ein erster Überblick erlangt werden über das Verständnis der ganzen Klasse von einem bestimmten Thema.
      Zudem kann im Anschluss die sowieso schon knappe verfügbare Zeit sehr gezielt für die Nachbesprechung von Fragen investiert werden, bei denen tatsächlich auch Nachholbedarf besteht.
      
      Des Weiteren ist beim Quiz der Aspekt der Anonymität als äusserst positiv hervorzuheben.
      Die Rücklaufquote bei diesen Quizes lag in den vergangenen Unterrichtswochen jeweils bei 100\% oder knapp darunter.
      Es liegt auf der Hand, dass genau diese Anonymität allen Studierenden ermöglicht, mit abzustimmen (auch denjenigen Studierenden, die sich ihrer Antwort vielleicht nicht ganz sicher sind und sich nicht vor der Klasse blossstellen möchten).
      Ein kleiner Nachteil der gewährten Anonymität ist natürlich, dass man dann nicht gezielt auf die Verständnisschwierigkeiten eines einzelnen Studierenden eingehen kann.
      Dieser Umstand ist aber nicht besonders tragisch, weil es sich sowieso lohnt, eine schlecht beantwortete Frage im Plenum noch einmal zu thematisieren.
      Nicht selten erhält dadurch ein anderer Studierender die wertvolle Gelegenheit, selbstständig eine Erklärung für seine Kommilitonen zu formulieren.
      Es ist zu erwarten, dass sich deshalb auch in der Prüfungslektion alle (oder fast alle) Studierenden wieder an dem Quiz beteiligen werden.
  \end{enumerate}
  
  \subsection{Unterrichtsverlauf}
  
  \begin{itemize}
    \item
      Rückblick über alte Themen und Ausblick auf neue Themen und deren Einordnung:
      \begin{itemize}
        \item
          Zeit: 5--10 Minuten.
        \item
          Didaktische Funktion:
          Wiederholung der wichtigsten Begriffe~\emph{Container},~\emph{Iteratoren} und~\emph{Algorithmen} aus der Unterrichtssequenz von vergangener Woche.
          Gleichzeitige Einordung und intuitive Erklärung der neuen Begriffe~\emph{Funktor},~\emph{Lambda-Funktion} und~\emph{Ranges}.
        \item
          Lehreraktivitäten:
          Tafelanschrieb und begleitende Erklärungen.
        \item
          Schüleraktivitäten:
          Formulierung von Verständnisfragen (falls vorhanden).
        \item
          Sozialform:
          Klassengespräch.
        \item
          Medien:
          Schematische Darstellung des Themenkomplexes, an der Wandtafel.
        \end{itemize}
    \item
      Interaktives Quiz mit Fragen über relevantes Vorwissen aus vergangenen Wochen:
      \begin{itemize}
        \item
          Zeit: 10--15 Minuten.
        \item
          Didaktische Funktion:
          Aktivierung von relevantem Vorwissen über \emph{Container}, \emph{Iteratoren} und \emph{Algorithmen} und gleichzeitiges Überprüfen von Verständnislücken mit anschliessender Diskussion.
        \item
          Lehreraktivitäten:
          Animierung der Studierenden zur Bearbeitung der Quiz-Fragen und anschliessende Diskussion der Resultate.
        \item
          Schüleraktivitäten:
          Bearbeitung der Quiz-Fragen und allfällige Formulierung von Erklärungen für die Kommilitonen.
        \item
          Sozialform:
          Einzelarbeit kombiniert mit Klassengespräch.
        \item
          Medien:
          Interaktives Quiz mit fünf Multiple-Choice-Fragen, angezeigt über den Projektor.
      \end{itemize}
  \end{itemize}
  
  
  \newpage\phantom{.}\vspace{-0.75cm}
  \section{Prüfungslektion: Funktoren und Lambda-Funktionen}
  
  \subsection{Bild der Studierenden}
  
  \begin{enumerate}[label=\alph*)]
    \item
      Vorkenntnisse:
      Die für beide Prüfungslektionen gleichermassen relevanten Vorkenntnisse über Container, Iteratoren und Algorithmen wurden bereits im Abschnitt~\ref{0:vorwissen} beschrieben.
      
      Eine weitere, speziell für diese erste Prüfungslektion relevante Vorkenntnis betrifft das \emph{Überladen von Operatoren} bei Klassen.
      Die in dieser Lektion behandelten \emph{Funktoren} sind nämlich nichts anderes als Klassen mit einem überladenen Funktions-Applikations-Operator.
      Eine Reihe von unterschiedlichen überladbaren Operatoren wurden in einer früheren Unterrichtswoche bereits thematisiert und eingeübt.
      Insbesondere mussten die Studierenden als Teil einer Übungsaufgabe einen eigenen Index-Operator implementieren.
      Da sich ein Index-Operator syntaktisch fast nicht von einem Funktions-Applikations-Operator mit einem Argument unterscheidet, sollten die Studierenden eigentlich das nötige Rüstzeug bereits mitbringen, um das Konzept und die Implementation eines Funktors mit einem Argument sehr schnell zu verstehen.
    \item
      Klassenspezifisches:
      Auch wenn das Überladen von Operatoren für eigene Klassen bereits in der Mitte des Semesters ausgiebig thematisiert und eingeübt wurde, darf nicht vergessen werden, dass die kognitiven Voraussetzungen der Studierenden doch sehr heterogen sind.
      Man muss deshalb davon ausgehen, dass ein nicht unbedeutender Anteil der Klasse das Überladen von Operatoren nicht mehr so gut beherrscht, wie man sich das vielleicht wünschen würde.
      Beim methodischen Vorgehen soll deshalb ein besonderes Augenmerk darauf gelegt werden, dass die betroffenen Studierenden an dieser Stelle nicht den Anschluss verlieren. 
  \end{enumerate}
  
  \subsection{Sachanalyse}
  \label{1:fachlich}

  \begin{enumerate}[label=\alph*)]
    \item
      Fachwissenschaftlicher Zusammenhang:
      Bei dem ursprünglich mit der Abkürzung STL (Standard Template Library) bezeichneten Teil der Standard-Bibliothek (das heisst, der Teil mit Containern, Iteratoren und Algorithmen) handelt es sich um einen der grundlegendsten und wichtigsten Komponenten.
      Durch die Abstrahierung von unterschiedlichen Containern mit Hilfe von Iteratoren und durch deren Anwendung als Argumente von Algorithmen wird eine Flexibilität und Ausdruckskraft erreicht, die seinesgleichen sucht.
      Aus diesem Grund liegt es auch auf der Hand, dass in einem Kurs über C++ zumindest die Grundzüge der ursprünglichen STL thematisiert werden sollten.
      
      Diese erwähnte Flexibilität und Ausdruckskraft kann jedoch noch weiter verstärkt werden, wenn neben Iteratoren und Werten auch Funktionsobjekte als Argumente von Algorithmen eingesetzt werden.
      Beispielsweise können durch den Einsatz von Prädikaten ganz einfach alle Werte mit einer bestimmten Eigenschaft innerhalb einer Sequenz gezählt werden.
      Oder durch den Einsatz verschiedener Komparatoren kann eine Sequenz von Werten mit dem gleichen Algorithmus nach unterschiedlichen Kriterien sortiert werden.
      Es ist deshalb äusserst wertvoll, direkt anschliessend an die Grundzüge der STL auch noch die unterschiedlichen Arten von Funktionsobjekten (wie Funktoren und Lambda-Funktionen) zu behandeln und deren Anwendung in Kombination mit Algorithmen zu thematisieren.
    \item
      Konzepte:
      Ein \emph{Funktor} ist ein Objekt (das heisst, eine Instanz) einer Klasse, für die der Funktions-Applikations-Operator überladen wurde.
      Das heisst einerseits, dass sich ein Funktor wie ein normales Objekt verhält, das durch den Aufruf eines Konstruktors kreiert wird, das möglicherweise eigene Attribute mit sich verändernden Werten hat, und das am Ende seiner Lebzeit durch den Aufruf eines Destruktors zerstört wird.
      Andererseits heisst das aber auch, dass ein Funktor mit der gleichen Syntax wie eine Funktion (sowohl mit als auch ohne Argumente) aufgerufen wird.
      Weil Funktoren im Gegensatz zu herkömmlichen Funktionen (oder Funktionszeigern) über einen eigenen Typ verfügen, eignen sie sich besonders gut für die Anwendung als generische Typ-Parameter von Funktionsobjekten bei Algorithmen.
      Der eigene Typ führt nämlich immer zu einer separaten Template-Instanziierung des Algorithmus, was insbesondere bei kleinen und oft aufzurufenden Funktionen (wie beispielsweise beim Komparator des Sortier-Algorithmus) wegen Inlining zu effizienterem Code führt.
      Ein weiterer potenzieller Vorteil, den Funktoren über herkömmliche Funktionen haben, ist, dass sie durch ihre Attribute über einen veränderbaren inneren Status verfügen.
      
      Eine \emph{Lambda-Funktion} ist im Prinzip nichts weiteres als syntaktischer Zucker, mit dem ein Funktor implementiert werden kann.
      Neben einem signifikant kleineren Schreibaufwand hat diese neue Syntax den entscheidenden Vorteil, dass ein Funktor-Objekt an Ort und Stelle als eigener Ausdruck kreiert werden kann.
      Insbesondere kann direkt in der Argument-Liste des Aufrufs eines Algorithmus ein solcher Ausdruck hingeschrieben werden.
      Die Verwendung von Lambda-Funktionen eignet sich deshalb besonders bei der Anwendung von Algorithmen mit Prädikaten, Komparatoren oder anderen Funktoren, die nur an einer einzigen Stelle im Code verwendet werden.
      Ein weiterer Vorteil dieser neuen Syntax ist, dass der Code des Funktionsaufrufs an der gleichen Stelle hingeschrieben werden kann, an der er dann auch aufgerufen wird.
  \end{enumerate}
  
  \subsection{Lernziele}

  \begin{enumerate}[label=\alph*)]
    \item
      Leitidee:
      Eine sehr grosse Anzahl von Algorithmen aus der Standard-Bibliothek verwendet als Teil ihrer Signatur Funktionsobjekte.
      Die Studierenden sollen deshalb ihr bereits vorhandenes Verständnis von Algorithmen und Iteratoren vertiefen, indem sie den Umgang mit Funktionsobjekten (Funktoren und Lambda-Funktionen) erlernen und diese zusammen mit Algorithmen aus der Standard-Bibliothek zur Anwendung bringen.  
    \item
      Dispositionsziel:
      Die Studierenden verwenden in ihren eigenen Programmen Algorithmen aus der Standard-Bibliothek, kombiniert mit eigens implementierten Funktionsobjekten, beispielsweise um einen Container nach einem bestimmten Kriterium zu sortieren.
    \item
      Operationalisierte Lernziele:
      \begin{itemize}
        \item
          Die Studierenden benennen drei wichtige Unterschiede zwischen herkömmlichen Funktionen, Funktoren und Lambda-Funktionen.
        \item
          Die Studierenden verwenden die korrekte Syntax, um einen Funktor mit oder ohne Attribute und mit bis zu zwei Argumenten zu implementieren.
        \item
          Die Studierenden verwenden die korrekte Syntax, um eine Lambda-Funktion mit bis zu zwei Argumenten zu implementieren.
        \item
          Die Studierenden verwenden die korrekte Syntax, um einen bekannten Algorithmus aus der Standard-Bibliothek mit einem eigens implementierten Funktionsobjekt aufzurufen.
      \end{itemize}
  \end{enumerate}
  
  \subsection{Didaktisch-methodische Überlegungen}
 
  \begin{enumerate}[label=\alph*)]
    \item
      Didaktischer Aufbau:
      Zunächst wird ein Repertoire von Algorithmen mit Funktionsobjekt als Argument aufgebaut, die dann für den Rest der Unterrichtssequenz verwendet werden können.
      Da normale Funktionen den Studierenden bereits bekannt sind, werden zunächst auch nur normale Funktionen verwendet.
      Im Anschluss wird eines der bereits gezeigten Beispiele angepasst und mit einem Funktor neu implementiert.
      Auf diese Weise sehen die Studierenden zum erstem Mal die Syntax, um Funktoren zu schreiben.
      
      Die darauffolgenden drei kleinen Übungsaufgaben, die bewusst unterschiedlich schwierig gewählt sind und von denen jeder Studierende eine auswählen soll, dienen zur Vertiefung des gerade gelernten neuen Materials.
      Anhand dieser Aufgaben soll den Studierenden klargemacht werden, dass Funktoren genauso wie Funktionen auch ihre Argumente per Referenz nehmen können, dass sie ebenso mehrere Argumente haben können, aber dass sie im Gegensatz zu Funktionen auch Attribute haben können.
      Danach wird eine kurze Nachbesprechung der Aufgaben durchgeführt.
      
      Zuletzt wird die vereinfachte Syntax der Lambda-Funktionen eingeführt und dazu verwendet, die drei bereits vorgelösten Aufgaben noch einmal mit viel weniger Schreibarbeit zu implementieren.
      Den Studierenden soll aber klar gemacht werden, dass es sich hierbei lediglich um syntaktischen Zucker für Funktoren handelt.
    \item
      Methodisches Vorgehen:
      Sowohl die neuen Algorithmen mit Funktionsobjekten, als auch die Syntax für Funktoren und die Syntax für Lambda-Funktionen sind den Studierenden noch unbekannt.
      Für die Übermittlung all dieser neuen Informationen bietet sich deshalb in erster Linie ein durch die Lehrperson geleitetes Klassengespräch an.
      Die Frage hingegen, in welcher Weise sich Funktoren von Funktionen unterscheiden, kann von den Studierenden für kurze Zeit selbstständig mit einer kleinen Aufgabe erforscht werden.
      Die Erwartung ist keineswegs, dass alle Studierenden jeweils alle gewünschten Erkenntnisse haben werden.
      Deshalb ist es wichtig, im Anschluss an die Aufgaben eine Nachbesprechung und Reflektion im Klassengespräch durchzuführen.
  \end{enumerate}
  
  \newpage\phantom{.}\vspace{-0.75cm}
  \subsection{Unterrichtsverlauf}

  \begin{itemize}
    \item
      Initiation mit Beispielen von Algorithmen, Funktionen und Funktoren:
      \begin{itemize}
        \item
          Zeit: 10--15 Minuten.
        \item
          Didaktische Funktion:
          Ein Repertoire von Algorithmen, die mit Funktionsobjekten arbeiten, soll aufgebaut werden.
          Dieses beinhaltet insbesondere~\codeinline{for_each},~\codeinline{count_if} und~\codeinline{sort}.
          Zudem sollen die Studierenden zum ersten Mal die Syntax für die Implementation eines Funktors zu sehen bekommen.
        \item
          Lehreraktivitäten:
          Erklärung und Vorführen von Algorithmen und Funktoren am Computer.
        \item
          Schüleraktivitäten:
          Paralleles Experimentieren am eigenen Computer, Verständnisfragen.
        \item
          Sozialform:
          Klassengespräch.
        \item
          Medien:
          Während dem Unterricht neu geschriebener Code, angezeigt über den Projektor.
        \end{itemize}
    \item
      Selbstständige Bearbeitung einer kleinen Aufgabe über Funktoren mit anschliessender Nachbesprechung (bei zu knapper verbleibender Zeit: direkte gemeinsame Besprechung):
      \begin{itemize}
        \item
          Zeit: 10--15 Minuten.
        \item
          Didaktische Funktion:
          Durch selbstständiges Experimentieren und Anwenden soll das gerade eben gelernte Wissen über Funktoren vertieft und erweitert werden.
        \item
          Lehreraktivitäten:
          Vorgabe von drei kleinen Aufgaben, anschliessende Nachbesprechung.
        \item
          Schüleraktivitäten:
          Auswahl und Bearbeitung einer von drei Aufgaben.
        \item
          Sozialform:
          Einzel- oder Zweierarbeit mit anschliessendem Klassengespräch.
        \item
          Medien:
          Aufgabenstellung in schriftlicher Form, angezeigt über den Projektor.
      \end{itemize}
    \item
      Erklärung von Lambda-Funktionen im Plenum:
      \begin{itemize}
        \item
          Zeit: 5--10 Minuten.
        \item
          Didaktische Funktion:
          Der Begriff und die Syntax von Lambda-Funktionen wird den Studierenden anhand von angepassten Lösungen zu den drei kleinen Aufgaben nähergebracht.
        \item
          Lehreraktivitäten:
          Erklärung und Vorführen von Lambda-Funktionen kombiniert mit den schon bekannten Algorithmen am Computer.
        \item
          Schüleraktivitäten:
          Paralleles Experimentieren am eigenen Computer, Verständnisfragen.
        \item
          Sozialform:
          Klassengespräch.
        \item
          Medien:
          Während dem Unterricht neu geschriebener Code, angezeigt über den Projektor.
      \end{itemize}
  \end{itemize}
  
  \section{Prüfungslektion: Ranges-Bibliothek}

  \subsection{Bild der Studierenden}
  
  \begin{enumerate}[label=\alph*)]
    \item
      Vorkenntnisse:
      Die für beide Prüfungslektionen gleichermassen relevanten Vorkenntnisse über Container, Iteratoren und Algorithmen wurden bereits im Abschnitt~\ref{0:vorwissen} beschrieben.
      
      Die einzige weitere relevante Vorkenntniss für diese zweite Prüfungslektion ist die Syntax für \emph{Funktoren} und \emph{Lambda-Funktionen} aus der gerade vorhergehenden ersten Prüfungslektion.
      Auch wenn natürlich nicht alle Studierenden ein gleich tiefes Verständnis dieser neuen Konzepte entwickelt haben, darf man trotzdem davon ausgehen, dass die Bedeutung der später im Unterricht verwendeten Lambda-Funktionen zumindest im passiven Sinne richtig erkannt wird.
    \item
      Klassenspezifisches:
      Auch hier sind die unterschiedlichen kognitiven Voraussetzungen der einzelnen Studierenden wieder relevant.
      Nicht alle werden die in der Lektion vorher behandelte Syntax für Lambda-Funktionen gleich präsent im Kopf haben.
      Es ist deshalb wichtig, auch hier auf die Schwächeren Rücksicht zu nehmen und gewisse Dinge vielleicht noch ein zweites Mal zu erklären.
  \end{enumerate}
  
  \newpage\phantom{.}\vspace{-0.75cm}
  \subsection{Sachanalyse}
  \label{2:fachlich}

  \begin{enumerate}[label=\alph*)]
    \item
      Fachwissenschaftlicher Zusammenhang:
      Bei den Ranges handelt es sich um ein relativ neues Feature, das erst seit C++20 zur Verfügung steht und in zukünftigen Standards der Programmiersprache auch  noch weiterentwickelt werden soll.
      Im Gegensatz zu den altbekannten Iteratoren-Paaren ermöglicht das Konzept der Ranges, Algorithmen aus der Standard-Bibliothek mit einem einzigen Argument anzusteuern.
      Diese Idee erscheint sehr natürlich, wenn man beispielsweise bemerkt, dass mit einem Sortier-Algorithmus oft ein ganzer Container (und nicht eine durch Iteratoren spezifizierte Teilsequenz) sortiert werden soll.
      Neben dieser vereinfachten Syntax für oft wiederkehrende Anwendungen haben Ranges aber noch eine Reihe von anderen Vorteilen.
      Ihre konsequente Verwendung hat einen positiven Effekt auf die Typsicherheit eines Programms, weil nicht mehr versehentlich zwei nicht zusammenpassende Iteratoren zu einem Paar kombiniert werden können.
      Auch bieten sich einzelne Range-Objekte viel mehr an für die Komposition mit den so genannten Range-Adaptors, was die bereits sehr grosse Flexibilität und Ausdruckskraft der Standard-Bibliothek noch weiter erhöht.
      Es liegt deshalb auf der Hand, die Studierenden an dieser neuen, spannenden und vermutlich wegweisenden Entwicklung der Standard-Bibliothek bereits jetzt teilhaben zu lassen.
    \item
      Konzepte: Eine \emph{Range} ist in erster Linie ein einzelnes Objekt, das innerhalb einer Range-based For-Schleife verwendet werden kann.
      Bereits bekannte Beispiele dafür sind die allermeisten Container aus der Standard-Bibliothek.
      Allgemeiner und ein bisschen vereinfacht kann man aber sagen, dass sich eine Range dadurch auszeichnet, dass sie einen Begin- und einen End-Iterator zur Verfügung stellt und dadurch eine Iteration ermöglicht.
      Neben den schon bekannten Containern können mit sogenannten Views und Range-Adaptors weitere Ranges erstellt und modifiziert werden.
      
      Eine \emph{View} ist ein leichtgewichtiges Objekt, das eine Range repräsentiert.
      Das Adjektiv leichtgewichtig ist so zu verstehen, dass keine explizite Repräsentation dieser Range im Speicher existiert.
      Einige sehr einfache Views können mit einer Auswahl von Factory-Funktionen aus der Standard-Bibliothek erstellt werden.
      Des Weiteren können aber auch kompliziertere Views durch Kombination von Range-Adaptors mit schon bestehenden Ranges (wie zum Beispiel Container) erstellt werden.
      
      Ein \emph{Range-Adaptor} ist ein Objekt, das mit einer gegebenen Range kombiniert werden kann und dadurch die gegebene Range auf unterschiedliche Weise modifiziert.
      Da es sich hierbei aber um eine leichtgewichtige View handelt, wird die Modifikation immer nur elementweise direkt beim späteren Durchiterieren ausgeführt.
      Eine vielleicht überraschende Konsequenz von dieser Tatsache ist, dass Range-Adaptors sich auch gut mit unendlichen Ranges vertragen.
      
      Im Zusammenhang mit Range-Adaptors ist auch noch der Begriff der \emph{Pipeline} zu erwähnen.
      Dieser beschreibt den Umstand, dass gleich mehrere Range-Adaptors kombiniert und hintereinander mit dem Operator~\codeinline{|} an eine gegebene Range gekettet werden können.
      Durch solche Pipelines können verschiedene Range-Adaptors im Prinzip beliebig zusammengesetzt werden, was einen enormen Gewinn an Flexibilität und Ausdruckskraft mit sich bringt.
  \end{enumerate}
  
  \subsection{Lernziele}

  \begin{enumerate}[label=\alph*)]
    \item
      Leitidee:
      In C++20 wurden die sogenannten Ranges als moderne, mächtigere und in vielen Fällen einfacher zu benutzende Alternative zu den schon seit mehreren Jahrzehnten bekannten Iteratoren-Paaren eingeführt.
      Die Studierenden sollen deshalb einen ersten Einblick in diese neue, wegweisende und sicherlich noch nicht abgeschlossene Entwicklung der Programmiersprache erhalten.
    \item
      Dispositionsziel:
      Die Studierenden sind sich den Vorzügen von Ranges über Iteratoren-Paare bewusst und verwenden deshalb die Ranges-Bibliothek in ihren eigenen Programmen, beispielsweise um mit einem Filter über eine ausgewählte Teilsequenz eines Containers zu iterieren.
    \item
      Operationalisierte Lernziele:
      \begin{itemize}
        \item
          Die Studierenden benennen mindestens zwei Vorteile, die das neue Konzept der Ranges über die klassischen Iteratoren-Paare aufweist.
        \item
          Die Studierenden erklären den Unterschied zwischen leichtgewichtigen Views und der schwergewichtigen Anwendung von Ranges-Algorithmen auf einen Container. 
        \item
          Die Studierenden verwenden die korrekte Syntax, um mit den verschiedenen Range-Adaptors aus der Standard-Bibliothek eine gegebene Range zu modifizieren.
        \item
          Die Studierenden verwenden die korrekte Syntax, um mit den verschiedenen Funktionen aus der Standard-Bibliothek einfache Ranges ohne zugrundeliegenden Container zu erstellen.
      \end{itemize}
  \end{enumerate}
  
  \subsection{Didaktisch-methodische Überlegungen}
 
  \begin{enumerate}[label=\alph*)]
    \item
      Didaktischer Aufbau:
      Auch in dieser Lektion soll zunächst ein Repertoire von Bausteinen aus der Ranges-Library aufgebaut werden, die dann im späteren Unterrichtsverlauf immer wieder verwendet werden können.
      Für einen einfachen Einstieg für alle Studierende werden zuerst die bereits bekannten Konzepte des Vektors, des Sortier-Algorithmus (jetzt einfach in der Variante für Ranges), und der Range-based For-Schleife thematisiert.
      Anhand von weiterführenden Bausteinen werden nach und nach alle neuen Konzepte (wie Ranges, Views und Range-Adaptors) eingeführt.
      Ganz am Schluss soll als Vorbereitung für die darauffolgende Unterrichtssequenz erwähnt werden, dass mit Hilfe des Operators~\codeinline{|} diese Range-Adaptors im Prinzip beliebig kombiniert werden können.
      
      Die darauffolgenden drei kleinen Übungsaufgaben haben wieder sehr gezielt gewählte und darum unterschiedliche Schwierigkeitsgrade.
      Idealerweise sollten alle Studierenden eine Aufgabe finden, die ihren kognitiven Fähigkeiten entspricht.
      Der eigentliche Zweck der drei Aufgaben aber ist, dass selbstständig die Möglichkeit erforscht werden kann, wie durch einfache Kombination der bekannten Range-Adaptors mit Pipelines auch komplexere Aufgabenstellungen gelöst werden können.
      Im direkt darauffolgenden Klassengespräch werden die drei Aufgaben nach Bedarf kurz nachbesprochen.
    \item
      Methodisches Vorgehen:
      Auch in diesem Themenkomplex existiert eine Reihe von neuen Begriffen mit dazugehörigen Objekten aus der Standard-Bibliothek, die den Studierenden noch gänzlich unbekannt sind.
      Für eine zeiteffiziente Übermittlung dieser neuen Informationen kommt deshalb wieder nur ein Klassengespräch in Frage.
      
      Sobald sich die Studierenden aber einmal einen groben Überblick über das Thema und die weiter oben genannten Bausteine verschafft haben, bieten sich Pipelines von bekannten Range-Adaptors für das selbstständige Experimentieren und Kombinieren an.
      Aufgrund der unterschiedlichen Schwierigkeitsgrade der Aufgaben besteht die Hoffnung, dass alle Studierenden ein kleines Erfolgserlebnis haben und ihr Wissen entsprechend vertiefen können.
      Die anschliessende Reflektion im Klassengespräch ist wichtig, um auf allfällige Schwierigkeiten einzugehen.
  \end{enumerate}
  
  \subsection{Unterrichtsverlauf}

  \begin{itemize}
    \item
      Erklärung von Ranges, Views, Range-Algorithmen und Range-Adaptors im Plenum:
      \begin{itemize}
        \item
          Zeit: 15--20 Minuten.
        \item
          Didaktische Funktion:
          Ein kleines Repertoire von Ranges und Range-Adaptors soll aufgebaut werden.
          Diese beinhalten insbesondere~\codeinline{iota},~\codeinline{istream},~\codeinline{filter},~\codeinline{transform}, \codeinline{take_while} und~\codeinline{reverse}.
          Zudem sollen die Studierenden anhand dieser Beispiele die wichtigsten Konzepte wie Ranges, Views, und Range-Adaptors zu verstehen lernen.
        \item
          Lehreraktivitäten:
          Erklärung und Vorführen von Ranges und Range-Adaptors am Computer.
        \item
          Schüleraktivitäten:
          Paralleles Experimentieren am eigenen Computer, Verständnisfragen.
        \item
          Sozialform:
          Klassengespräch.
        \item
          Medien:
          Während dem Unterricht neu geschriebener Code, angezeigt über den Projektor.
        \end{itemize}
    \item
      Selbstständige Bearbeitung einer kleinen Aufgabe über Pipelines mit anschliessender Nachbesprechung (bei zu knapper verbleibender Zeit: direkte gemeinsame Besprechung):
      \begin{itemize}
        \item
          Zeit: 10--15 Minuten.
        \item
          Didaktische Funktion:
          Durch selbstständiges Experimentieren und Kombinieren mit Pipelines soll das gerade eben gelernte Wissen über Ranges und Range-Adaptors vertieft werden.
        \item
          Lehreraktivitäten:
          Vorgabe von drei kleinen Aufgaben, anschliessende Nachbesprechung.
        \item
          Schüleraktivitäten:
          Auswahl und Bearbeitung einer von drei Aufgaben.
        \item
          Sozialform:
          Einzel- oder Zweierarbeit mit anschliessendem Klassengespräch.
        \item
          Medien:
          Aufgabenstellung in schriftlicher Form, angezeigt über den Projektor.
      \end{itemize}
  \end{itemize}
  
\end{document}