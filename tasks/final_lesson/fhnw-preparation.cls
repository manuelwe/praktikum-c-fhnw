\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fhnw-preparation}[2024/06/02 Vorbereitung für Prüfungslektionen für C++ Kurs an der FHNW]

\LoadClass[head=Prüfungslektionen]{../fhnw}
