\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fhnw-group}[2024/05/01 Lösung für Gruppenarbeit für C++ Kurs an der FHNW]

\LoadClass[head=Musterlösung~zu~Gruppenarbeit,number]{../fhnw}

\newfhnwenvironment{loesung}{Musterlösung}{\fhnwhaken}
