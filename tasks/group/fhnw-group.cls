\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fhnw-group}[2024/05/01 Gruppenarbeit für C++ Kurs an der FHNW]

\LoadClass[head=Gruppenarbeit,number]{../fhnw}

\newfhnwenvironment{aufgabe}{Gruppenarbeit}{\fhnwflasche}
\newfhnwenvironment{loesung}{Lösung}{\fhnwhaken}
