\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{fhnw-3fold}[2024/06/03 3Fold-Problem für C++ Kurs an der FHNW]

\LoadClass[head=Drei~kleine~Lernaufgaben]{../fhnw}

\newfhnwenvironment{aufgabe}{Lernaufgaben}{\fhnwflasche}
\newfhnwenvironment{loesung}{Musterlösungen}{\fhnwhaken}
