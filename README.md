# Praktikum C++ FHNW


Alles erstellte Material für das Unterrichtspraktikum an der FHNW im Frühling 2024 bei Prof. Dr. Christoph Stamm zum Thema C++.

Folgender Befehl erstellt alle Unterrichtsmaterialien in den entsprechenden Unterordnern unter `tasks/.`, sowie den Bericht unter `report/.`
```
make
```
 
