include Makelib

TASKS  := tasks/
REPORT := report/

.PHONY: all
all: report

.PHONY: tasks
tasks:
	@$(subst WHERE,$(TASKS),$(RECURSE))

.PHONY: report
report:
	@$(subst WHERE,$(REPORT),$(RECURSE))
